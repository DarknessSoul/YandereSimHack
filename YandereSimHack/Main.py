from pymem import Pymem
from pymem.process import module_from_name
from pymem.ptypes import RemotePointer
import time 
def GetProcessHandle():
    pymemyand = Pymem("YandereSimulator.exe")
    return pymemyand.process_handle
def YandereSim():
    pm = Pymem("YandereSimulator.exe")
    return pm
def GetMonoBehaviour():
    return module_from_name(GetProcessHandle(), "{}.dll".format("mono-2.0-bdwgc")).lpBaseOfDll
def GetRemotePointerAddr(base, offsets):
    remote_pointer = RemotePointer(YandereSim().process_handle, base)
    for offset in offsets:
        if offset != offsets[-1]:
            remote_pointer = RemotePointer(YandereSim().process_handle, remote_pointer.value + offset)
        else:
            return remote_pointer.value + offset

def Main():
    pymem = Pymem("YandereSimulator.exe")
    pointsread = GetRemotePointerAddr(GetMonoBehaviour() + 0x00497E28, offsets=[0x80, 0xD60, 0x194])
    points = pymem.read_uint(pointsread)
    print(points)
    pymem.write_uint(pointsread, 70000)
    time.sleep(5)
    print(pointsread)
    exit(551)
if __name__ == "__main__":
    Main()